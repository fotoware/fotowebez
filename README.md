# Fotoware EZ Publishing Plugin Documentation #

This plugin is created to work with both eZ Publish 4.x and eZ Publish 5.x. For 5.x the plugin can run as a legacy extension but can be used in both old stack and new stack templates.

### Installation ###

Download the extension and place it in the extension/ folder (eZ Publish 4), or in ezpublish_legacy/extension/ (eZ Publish 5).
Execute autoload script for eZ Publish:
“php bin/php/ezpgenerateautoloads.php” while being placed in extension/ folder (eZ Publish 4), or in ezpublish_legacy/extension/ (eZ Publish 5).


### Update database ###

Inside the extension folder we have a SQL file called fotoware.sql. Insert this into your database. The content of this file is below and can also be copy/pasted into the SQL console.
The SQL code in the fotoware.sql file:

```
#!sql
CREATE TABLE IF NOT EXISTS `ezfotoware` (
  `id` int(11) NOT NULL,
  `object_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `href` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `ezfotoware`
  ADD PRIMARY KEY (`object_id`,`attribute_id`),
  ADD KEY `id` (`id`) USING BTREE;

ALTER TABLE `ezfotoware`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;


```


### Configuration ###

If you want to enable Single Sign-On per user basis you need to add an attribute on the User class in eZ Publish (Tab “Setup” > “Classes” > Class groups “Users” > Edit class “User”).
Add a “text line”new attribute. We recommend using the name “FotoWeb username” and identifier is “fotoweb_username”. The name of the identifier can be changed, but then the configuration file needs to be updated.
![1.png](https://bitbucket.org/repo/KyLG5e/images/2998817159-1.png)
All users who want access to FotoWeb can then edit their own user object and insert their FotoWeb username.
![2.png](https://bitbucket.org/repo/KyLG5e/images/2464641662-2.png)

#### Structure of folder eZ Publish with the “ezfotoware” extension ####

![3.png](https://bitbucket.org/repo/KyLG5e/images/1831798140-3.png)

#### Edit the file fotoware.ini.append.php and set FotoWare configuration ####
![4.png](https://bitbucket.org/repo/KyLG5e/images/1160468373-4.png)

File path: <ezroot>/extension/ezfotoware/settings/fotoware.ini.append.php

#### The “[LoginSettings]”block has the SSO configuration ####
* “Server” is link of the Fotoware server. Add the link “demo.fotoware.com”.
* “APIToken” is the token of API. Add the token “12345abcdef”.
* “UserLogin” is the user of the Fotoware. Add the user “steinarm”.
* “UserPassword” is the password of the Fotoware. Add the password “steinar123#m”.
* “FotoWebEncryptionSecret” is the generated code by API. Add the code “vTAP4SckcFFRJvlCFIBjHVj9OE98Jp”.
* “FotoWebUsernameAttribute” is the identifier of User class. Add the name “fotoweb_username”.

#### The “[setupSettings]” have the configuration of URLs of the Fotoweb Widgets ####
* “UrlFotowareBasic” is the basic URL from Fotoware server. Add the URL “http://demo.fotoware.com”.
* “UrlWidgetSelection” is the path URL from Widget Select. Add the path “/fotoweb/widgets/selection”.
* “UrlWidgetEdit” is the path URL from Widget Edit Image. Add the path “/fotoweb/widgets/publish?i=”.

#### eZ 4.7 - Template ####
For render the images in template:

```
#!php

{attribute_view_gui attribute=$node.data_map.image    image_class='articleimage'}
```

#### eZ 5.4 - Template ####

```
#!json

{{ ez_render_field( content, "image",
     {
"template": "eZDemoBundle:fields:ezimage_simple.html.twig",
"parameters": {"alias": image_alias, "alt": content_name}
     }
)}}

```
#### Expire Images – JSON Resquests ####
Send a "POST" request to link:
**http://<site domain>/fotoweb/action**

The “POST“ request should have a variable called “json” with format below:
E.g:

```
#!json

{ 
"action": "retract", 
	"objects": [ 
{ "href" : "/fotoweb/archives/5003-Stock%20Photo/Stock%20Photo/Extreme%20Sports/75%20CMYK/dv617087.jpg.info" }, 
{ "href" : "/fotoweb/archives/5003-Stock%20Photo/Stock%20Photo/Extreme%20Sports/75%20CMYK/dv617091.jpg.info" }
	],
	"replaceto": "http://www.archersafetysigns.co.uk/images/g/g1048428.gif"
}

```
Description of the JSON:
* "action” -> action of the request. The value of action has to be “retract”.
* "objects" -> array of ID Fotoweb images that will be replaced.
* "href" -> ID of Fotoweb image. All images from “objects” parameter will be replaced by image in “replaceto” parameter.
* "replaceto"-> image URL.

The return of request is:

```
#!json

[
{"success":true,"href":"\/fotoweb\/archives\/5003-Stock%20Photo\/Stock%20Photo\/Extreme%20Sports\/75%20CMYK\/dv617087.jpg.info","imageold":"var\/ezdemo_site\/storage\/images\/media\/images\/dv617087.jpg7\/1662-11-eng-GB\/dv617087.jpg.jpg"},
{"success":true,"href":"\/fotoweb\/archives\/5003-Stock%20Photo\/Stock%20Photo\/Extreme%20Sports\/75%20CMYK\/dv617087.jpg.info","imageold":"var\/ezdemo_site\/storage\/images\/media\/images\/dv617087.jpg3\/1642-1-eng-GB\/dv617087.jpg.jpg"},
{"success":true,"href":"\/fotoweb\/archives\/5003-Stock%20Photo\/Stock%20Photo\/Extreme%20Sports\/75%20CMYK\/dv617087.jpg.info","imageold":"var\/ezdemo_site\/storage\/images\/media\/images\/dv617087.jpg2\/923-42-eng-GB\/dv617087.jpg.jpg”} ]

```