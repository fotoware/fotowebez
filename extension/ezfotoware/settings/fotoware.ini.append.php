<?php /*

[LoginSettings]
Server=<insert host name>
APIToken=<insert API token>
UserLogin=<insert user login>
UserPassword=<insert user password>
FotoWebEncryptionSecret=<insert encryption secret>
FotoWebUsernameAttribute=fotoweb_username

[setupSettings]
UrlWidgetSelection=/fotoweb/widgets/selection
UrlWidgetEdit=/fotoweb/widgets/publish?i=
UrlFotowareBasic=<insert protocol (http/https) +  hostname>


*/?>