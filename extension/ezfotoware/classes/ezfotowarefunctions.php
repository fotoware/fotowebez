<?php

class ezFotowareFunctions {

	public static function revoke( $json = null, $UrlFotowareBasic = null ) {
		
		$result = array();
		
		if( !empty( $json->source ) and !empty( $json->normal ) ){
				
			$source = str_replace( $UrlFotowareBasic, "", $json->source );
			$cond = array( 'href' => $source );
			$objlist = eZPersistentObject::fetchObjectList( ezFotowareObject::definition(), null, $cond );
		
			if( !empty( $objlist ) ){
		
				$arr      = explode( '|', trim( $json->normal ) );
				$source   = str_replace( ' ', '%20', $arr[0] );
				$replaceto  = "";
		
				if( file_exists( $source ) ) {
					// Handle local files
					$replaceto = $source;
				}else{
					// Handle remote files
					$filename = 'var/cache/'. md5( microtime() ) . substr( $source, strrpos( $source, '.' ) );
					if( !empty( $source ) ) {
						if( in_array( 'curl', get_loaded_extensions() ) ) {
							$ch = curl_init();
							$out = fopen( $filename, 'w' );
							curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1 );
							curl_setopt( $ch, CURLOPT_URL, $source );
							curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
							curl_setopt( $ch, CURLOPT_FILE, $out );
							curl_exec( $ch );
							curl_close( $ch );
							fclose( $out );
		
						} else {
							copy( $source, $filename );
						}
							
					}
		
					if( file_exists( $filename ) ) {
						$replaceto = $filename;
					}
				}
		
				if( !empty( $replaceto ) ){
		
					foreach ( $objlist as $obj){
						$contentObject = eZContentObject::fetch( $obj->attribute( 'object_id' ), true);
						$contentObjectAttribute = eZContentObjectAttribute::fetch( $obj->attribute( 'attribute_id' ), $contentObject->attribute( 'current_version' ) );
						$attrIdentifier = $contentObjectAttribute->contentClassAttributeIdentifier();
							
						$dataMap =& $contentObject->attribute( 'data_map' );
						$image =& $dataMap[ $attrIdentifier ]->content();
						$list =& $image->aliasList();
							
						$params = array(
								'attributes'              => array(
										$attrIdentifier => $replaceto
								)
						);
							
						$objectUpdate = eZContentFunctions::updateAndPublishObject( $contentObject, $params );
							
						if( !empty( $objectUpdate )){
							array_push( $result, array('success' => true, 'href' => $json->source, "imageold" => $list['original']['url'] ) );
						}else{
							array_push( $result, array('success' => false, 'href' => $json->source, "imageold" => $list['original']['url'] ) );
						}
					}
				}
		
				if( file_exists( $filename ) ) {
					unlink( $filename );
				}
			}
				
			return json_encode( $result );
				
		}else{
			return json_encode(array('success' => false, 'text-error' => "The Source or Normal attributes are empty!"));
		}		
	}
}