<?php
 
class ezFotowareObject extends eZPersistentObject
{
     const STATUS_ACCEPTED = 1;
     const STATUS_PENDING = 0;
     /**
     * Construct, use {@link UserExpObject::create()} to create new objects.
     * 
     * @param array $row
     */
    protected function __construct(  $row )
    {
        parent::eZPersistentObject( $row );
    }
 
    public static function definition()
    {
        static $def = array( 'fields' => array(
                    'id' => array(
                                       'name' => 'id',
                                       'datatype' => 'integer',
                                       'default' => 0,
                                       'required' => true ),
                    'object_id' => array(
                                       'name' => 'object_id',
                                       'datatype' => 'integer',
                                       'default' => 0,
                                       'required' => true ),
	        		'attribute_id' => array(
				        				'name' => 'attribute_id',
				        				'datatype' => 'integer',
				        				'default' => 0,
				        				'required' => true ),
	        		'href' => array(
				        				'name' => 'href',
				        				'datatype' => 'string',
				        				'default' => '',
				        				'required' => true )
                  ),
                  'keys' => array( 'object_id', 'attribute_id'),
        		  'sort' => array('id' => 'desc'),
        		  'increment_key' => 'id',
                  'class_name' => 'ezFotowareObject',
                  'name' => 'ezfotoware' );
        
        return $def;
    }
    
    public static function create( array $row = array() )
    {
    	$object = new self( $row );
    	return $object;
    }
    
}

?>