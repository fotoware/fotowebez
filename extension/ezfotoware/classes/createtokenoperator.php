<?php 

class CreateTokenOperator
{
	
	/*!
	 Constructor
	 */
	function CreateTokenOperator()
	{
		$this->Operators = array( 'ezcreate_token');
	}
	
	/*!
	 Returns the operators in this class.
	 */
	function &operatorList()
	{
		return $this->Operators;
	}
	
	/*!
	 \return true to tell the template engine that the parameter list
	 exists per operator type, this is needed for operator classes
	 that have multiple operators.
	 */
	function namedParameterPerOperator()
	{
		return true;
	}
	
	/*!
	 The first operator has two parameters, the other has none.
	 See eZTemplateOperator::namedParameterList()
	 */
	function namedParameterList()
	{
		return array(
				'ezcreate_token' => array() );
	}
	
	/*!
	 Executes the needed operator(s).
	 Checks operator names, and calls the appropriate functions.
	 */
	function modify( &$tpl, &$operatorName, &$operatorParameters, &$rootNamespace,
			&$currentNamespace, &$operatorValue, &$namedParameters )
	{
		switch ( $operatorName )
		{
			case 'ezcreate_token':
				{
					$operatorValue = $this->ezcreate_token();					
				} break;
		}
	}
	
	function ezcreate_token()
	{
		$ini = eZINI::instance('fotoware.ini');
		
		$secret = '';
		if( $ini->hasVariable( 'LoginSettings', 'FotoWebEncryptionSecret' ) )
		{
			$secret = $ini->variable( 'LoginSettings', 'FotoWebEncryptionSecret' );
		}
		
		$fotoWebUserAttribute = '';
		if( $ini->hasVariable( 'LoginSettings', 'FotoWebUsernameAttribute' ) )
		{
			$fotoWebUserAttribute = $ini->variable( 'LoginSettings', 'FotoWebUsernameAttribute' );
		}
		        
		$loginToken = '';
		
		if (strlen($secret) > 0 and strlen($fotoWebUserAttribute) > 0)
		{
			$currentUser = eZUser::currentUser();
			
			$dataMap = $currentUser->contentObject()->dataMap();
			
			$fotoWebUser = $dataMap[$fotoWebUserAttribute]->content();
			
			$tokenGenerator = new LoginTokenGenerator($secret, true);
			$loginToken = $tokenGenerator->CreateLoginToken($fotoWebUser);
		}
		
		return $loginToken;
	}
	
	/// \privatesection
	var $Operators;
	
}


?>