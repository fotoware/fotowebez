<?php

	header('Content-Type: application/json; charset=utf-8');	
		
	// modul1/create.php – Function file of View create
	$module = $Params['Module'];
	 
	// take copy of global object
	$http = eZHTTPTool::instance ();
	
	//Displays the XML.
	$tpl = eZTemplate::factory();
	
	$cli = eZCLI::instance();
	
	
	if( $http->hasPostVariable('ftw_type') and $http->hasPostVariable('ftw_action') and 
	!empty( $http->variable ('ftw_type') ) and !empty( $http->variable ('ftw_action') ) and 
	!empty( $http->variable ('url_file') ) and !empty( $http->variable ('href') ) ){
		
		$ftw_object = $http->variable ('ftw_type');
		$ftw_action = $http->variable ('ftw_action');
		$object_id = $http->variable ('object_id');
		$attribute_id = $http->variable ('attribute_id');
		$version_id = $http->variable ('version_id');
		$url_file = $http->variable ('url_file');
		$href = $http->variable ('href');
		$caption = $http->variable ('ftw_caption');
		
		$user = eZUser::currentUser();
		$userID = eZUser::currentUserID();
		
		if( $ftw_object == 'uploadimage'){
			
			$contentObject = eZContentObject::fetch( $object_id);			
			$contentObjectAttribute = eZContentObjectAttribute::fetch( $attribute_id, $version_id );
			
			$arr      = explode( '|', trim( $url_file ) );
			$source   = str_replace( ' ', '%20', $arr[0] );
			
			if( file_exists( $source ) ) {
				// Handle local files
				$content = $contentObjectAttribute->attribute( 'content' );
				$content->initializeFromFile( $source, null );
				$content->store( $contentObjectAttribute );
			}else{
				// Handle remote files
				$filename = 'var/cache/'. md5( microtime() ) . substr( $source, strrpos( $source, '.' ) );
				if( !empty( $source ) ) {
					if( in_array( 'curl', get_loaded_extensions() ) ) {
						$ch = curl_init();
						$out = fopen( $filename, 'w' );
						curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1 );
						curl_setopt( $ch, CURLOPT_URL, $source );
						curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
						curl_setopt( $ch, CURLOPT_FILE, $out );
						curl_exec( $ch );
						curl_close( $ch );
						fclose( $out );
						
					} else {
						copy( $source, $filename );					
					}
				
				}
				
				if( file_exists( $filename ) ) {
					$content = $contentObjectAttribute->attribute( 'content' );
					$content->initializeFromFile( $filename, null );
					$content->store( $contentObjectAttribute );
										
					$cond = array( 'object_id' => $object_id, 'attribute_id' => $attribute_id );
					
					$data = array(
							'object_id' => $object_id,
							'attribute_id' => $attribute_id,
							'href' => $href
					);
						
					$obj = eZPersistentObject::fetchObject( ezFotowareObject::definition(), null, $cond );
					$objArray = (array) $obj;
						
					if( !empty( $objArray ) ){
						//update ezfotoware object.
						foreach ($data as $key => $value){
							$obj->setAttribute( $key, $value );
						}
							
						$obj->store();
					
					}else{
						//create ezfotoware object.
						$obj = ezFotowareObject::create( $data );
						$obj->store();
					}
					
					unlink( $filename );
					
					echo(json_encode(array('success' => true, 'saveobject' => $content )));
					
				}else{
					echo(json_encode(array('success' => false, 'saveobject' => false)));
				}
			}
			
		}elseif ($ftw_object == 'objectimage'){
			
			$pathInfo = array();
			
			if(!empty( $href )){
				
				$pathInfo = pathinfo( $href );
				//$objectContentRemote = eZContentObject::fetchByRemoteID( $href , true);
				
				if( !empty( $object_id ) ){
					
					$objectContentRemote = eZContentObject::fetch( $object_id , true);
					
					if( !empty( $objectContentRemote ) ){
						$ftw_action = "update";
						$object_id = $objectContentRemote->attribute( 'id' );
					}				
				}				
			}
			
			$arr      = explode( '|', trim( $url_file ) );
			$source   = str_replace( ' ', '%20', $arr[0] );
			$pathUrlLocal  = "";
				
			if( file_exists( $source ) ) {
				// Handle local files
				$pathUrlLocal = $source;
			}else{
				// Handle remote files
				$filename = 'var/cache/'. md5( microtime() ) . substr( $source, strrpos( $source, '.' ) );
				if( !empty( $source ) ) {
					if( in_array( 'curl', get_loaded_extensions() ) ) {
						$ch = curl_init();
						$out = fopen( $filename, 'w' );
						curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1 );
						curl_setopt( $ch, CURLOPT_URL, $source );
						curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
						curl_setopt( $ch, CURLOPT_FILE, $out );
						curl_exec( $ch );
						curl_close( $ch );
						fclose( $out );
			
					} else {
						copy( $source, $filename );
					}
			
				}
			
				if( file_exists( $filename ) ) {
					$pathUrlLocal = $filename;
				}
			}
			
			if($ftw_action == 'update'){
			
				$contentObject = eZContentObject::fetch( $object_id, true);
								
				//$datamap = $contentObject->DataMap->attribute('caption'); 
				$dataMap =& $contentObject->attribute( 'data_map' );
				 
				//caption attribute
				$captionDescription =& $dataMap['caption']->content();
				$captionDescriptionOutput =& $captionDescription->attribute('output');
				$captionDescriptionOutputText = $captionDescriptionOutput->attribute('output_text');
				
				if( !empty( $caption )){
					$parser = new eZSimplifiedXMLInputParser();
					$parser->setParseLineBreaks(true);
					$document = $parser->process($caption);
					$caption = eZXMLTextType::domString($document);
				}
				
				
				$attributeName =  $pathInfo['filename'];
				$attributeCaption = empty($caption)? $captionDescriptionOutputText : $caption ;
				
				$params = array(
						'attributes'              => array(
								'name' => $attributeName,
								'image' => $pathUrlLocal,
								'caption' => $attributeCaption
						)
				);
				
				$objectUpdate = eZContentFunctions::updateAndPublishObject( $contentObject, $params );
				
				
				if( !empty( $objectUpdate )){
					
					$imageAttribute =& $dataMap['image'];
					
					$cond = array(
							'object_id' => $contentObject->attribute( 'id' ),
							'attribute_id' => $imageAttribute->attribute( 'id' )
					);					
					
					$data = array(
							'object_id' => $contentObject->attribute( 'id' ),
							'attribute_id' => $imageAttribute->attribute( 'id' ),
							'href' => $href
					);
					
					$obj = eZPersistentObject::fetchObject( ezFotowareObject::definition(), null, $cond );
					$objArray = (array) $obj;
					
					if( !empty( $objArray ) ){
						//update ezfotoware object.
						foreach ($data as $key => $value){
							$obj->setAttribute( $key, $value );
						}
							
						$obj->store();
						
					}else{
						//create ezfotoware object.
						$obj = ezFotowareObject::create( $data );
						$obj->store();						
					}	
					
					echo(json_encode(array('success' => true, 'saveobject' => $contentObject->attribute( 'id' ) )));
				}else{
					echo(json_encode(array('success' => false, 'saveobject' => false)));
				}
				
				
			}elseif( $ftw_action == 'create' ){
								
				$parent_node = eZContentObjectTreeNode::fetchByURLPath( 'media/images' );				
				
				if( !empty( $caption )){
					$parser = new eZSimplifiedXMLInputParser();
					$parser->setParseLineBreaks(true);
					$document = $parser->process($caption);
					$caption = eZXMLTextType::domString($document);
				}
				
				$attributeName =  $pathInfo['filename'];
				$attributeCaption = $caption;
				
				$params = array(
						'class_identifier' => 'image',
						'parent_node_id'    => $parent_node->attribute( 'node_id' ),
						'attributes'      => array(
								'name' => $attributeName,
								'image' => $pathUrlLocal,
								'caption' => $attributeCaption
						)
				);
				
				$object = eZContentFunctions::createAndPublishObject( $params );
				
				if( !empty( $object )){
					
					$dataMap =& $object->attribute( 'data_map' );
					$imageAttribute =& $dataMap['image'];
					
					$data = array(
							'object_id' => $object->attribute( 'id' ),
							'attribute_id' => $imageAttribute->attribute( 'id' ),
							'href' => $href
					);
					
					//create ezfotoware object.
					$obj = ezFotowareObject::create( $data );
					$obj->store();
					
					echo(json_encode(array('success' => true, 'saveobject' => $object->attribute( 'id' ) )));
				}else{
					echo(json_encode(array('success' => false, 'saveobject' => false)));
				}
				
			}
			
			if( file_exists( $filename ) ) {
				unlink( $filename );
			}
			
			
		}else{
			echo(json_encode(array('success' => false, 'saveobject' => false)));
		}		
		
	}else{
		echo(json_encode(array('success' => false, 'saveobject' => false)));
	}
		
	
	eZExecution::cleanExit();

?>