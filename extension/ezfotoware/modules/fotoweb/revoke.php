<?php

	header('Content-Type: application/json; charset=utf-8');	
		
	// modul1/create.php – Function file of View create
	$module = $Params['Module'];
	 
	// take copy of global object
	$http = eZHTTPTool::instance();
	
	//Displays the XML.
	$tpl = eZTemplate::factory();
	
	$cli = eZCLI::instance();
	
	$ini = eZINI::instance( "fotoware.ini" );
	$UrlFotowareBasic = $ini->variable( "setupSettings", "UrlFotowareBasic" );
	
	if( $http->hasPostVariable('json') and !empty( $http->variable ('json') ) and !empty(json_decode( $http->variable ('json') )) ){

		$json =  json_decode( $http->variable ('json') );
		$result = ezFotowareFunctions::revoke( $json, $UrlFotowareBasic);
		
		echo $result;
		
	}else{		
		echo(json_encode(array('success' => false, 'text-error' => "json object invalid!")));		
	}
	
	eZExecution::cleanExit();

?>