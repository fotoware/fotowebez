<div id="fotoware-panel" class="panel" style="background: #fff;">

	<div id="fotoware_box_edit" style="display:none; position: relative;width:760px;height:410px;overflow:hidden;">
	    <div style="background-color: #eee; text-align: center">             
			<iframe src="" style="display:block; width:100%; height:430px; padding:0px; margin:0px; border:0px;background: #fff;"></iframe>	
	    </div>
	</div>

	<div id="fotoware_box" style="display:none; position: relative;width:760px;height:410px;overflow:hidden;">
	    <div style="background-color: #eee; text-align: center">             
			<iframe src="" style="display:block; width:100%; height:430px; padding:0px; margin:0px; border:0px;background: #fff;"></iframe>	
	    </div>
	</div>
		
</div>


{literal}  
<script type="text/javascript">
{/literal}

	var urlfotowarebasic = "{ezini( 'setupSettings', 'UrlFotowareBasic', 'fotoware.ini' )}";
	var urlwidgetselection = urlfotowarebasic +"{ezini( 'setupSettings', 'UrlWidgetSelection', 'fotoware.ini' )}";
	var urlwidgetedit = urlfotowarebasic + "{ezini( 'setupSettings', 'UrlWidgetEdit', 'fotoware.ini' )}";

	var assetImage;
	//var ezroot_url_admin = {'/'|ezurl()};
  	var ezroot_url_admin = "{concat("http://",ezini( 'SiteSettings', 'SiteURL'))}";

	var logintoken = "{ezcreate_token()}";

	if (logintoken.length > 0 )
		urlwidgetselection = urlwidgetselection + "?lt=" + logintoken; 

{literal}

//function of fotoware widget.
function listener(event){
	
	var url = urlfotowarebasic;
	
	if(event.origin !== url)
		return;
	
	var data = event.data;
	
	if(data.event === 'assetSelected')	
		listimage(data);

	if(data.event === 'assetExported')
		exportImage(data);
}

if(window.addEventListener){
	window.addEventListener("message", listener, false);
}else{
	window.attachEvent("onmessage", listener);
}


//get the object of image selectioned.
function listimage(data){	
	assetImage = data;	
	showFotowebEdit(data.asset.href);		
}


function exportImage(data){

	$("#fotoware_box_edit").hide();
	var embed_id;
	if( !( typeof tinyMCEPopup.params.selected_node.id === 'undefined') ){
		 var embedArray = tinyMCEPopup.params.selected_node.id.split("_");
		 embed_id = embedArray[1];
	}else{
		embed_id = "";
	}		
	
	var exported = data.export.export;
	var url_file = exported.image.normal;
	var href = assetImage.asset.href;
	var object_id = embed_id;
	var attribute_id = "";
	var attribute_version = "";
	var caption = data.export.caption.label;
			
	var sendInfo = {
	           'object_id': object_id,
	           'attribute_id': attribute_id,
	           'url_file': url_file,
	           'href': href,
	           'version_id': attribute_version,
	           'ftw_type': 'objectimage',
	           'ftw_action': 'create',
		       'ftw_caption': caption	           
	       };

	$.ajax({
	      url: ezroot_url_admin + '/fotoweb/saveobject',
	       type: 'post',
	       data: sendInfo,
	       dataType: 'json',
	       success: function (data){	    	    
				if(data.success == true){
					showObjectRelation( data.saveobject );
				}
	      }  
	});
}

function showObjectRelation( objectId ){
	eZOEPopupUtils.selectByEmbedId( objectId );
}

//this function show the fotoware iframe selection.
function showFotoware(){
	//create custom tag fotoware.
	$("#fotoware_box > div > iframe").attr("src", urlwidgetselection );
	$("#fotoware_box").show();
}


//this function show the fotoware iframe edit.
function showFotowebEdit( href ){
	
	var urliframe = urlwidgetedit + href;
	$("#fotoware_box_edit > div > iframe").attr("src", urliframe );

	$("#fotoware_box").hide();
	$("#fotoware_box_edit").show();
}

function hiddenScrollSizePopup(){
	
	$('#fotoware-panel').closest( "body" ).css( "overflow", "hidden");
	tinyMCEPopup.editor.windowManager.onOpen.scope.params.mce_width = 800;
	tinyMCEPopup.editor.windowManager.onOpen.scope.params.mce_height = 500;
	window.parent.centerIfr( tinyMCEPopup.editor.windowManager.params.mce_window_id, 800, 500);
	
}

$(document).ready(function() {

	showFotoware();
	hiddenScrollSizePopup();
	
});
</script>

{/literal}