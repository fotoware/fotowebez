{**** ep image editor *****}
{run-once}
    {if is_set($ezie_ajax_response)|not()}
        {include uri='design:ezie/gui.tpl' attribute=$attribute}
    {/if}
{/run-once}

{default attribute_base='ContentObjectAttribute'}
{let attribute_content=$attribute.content}

{literal}
<script type="text/javascript">
{/literal}
	//ezroot_url_admin = {'/'|ezurl()};
	ezroot_url_admin = "{concat("http://",ezini( 'SiteSettings', 'SiteURL'))}";
  	urlfotowarebasic = "{ezini( 'setupSettings', 'UrlFotowareBasic', 'fotoware.ini' )}";
  	urlwidgetselection = urlfotowarebasic +"{ezini( 'setupSettings', 'UrlWidgetSelection', 'fotoware.ini' )}";
	urlwidgetedit = urlfotowarebasic + "{ezini( 'setupSettings', 'UrlWidgetEdit', 'fotoware.ini' )}";

	logintoken = "{ezcreate_token()}";

	if (logintoken.length > 0 )
		urlwidgetselection = urlwidgetselection + "?lt=" + logintoken;

{literal}	
</script>
{/literal}

{* Current image. *}
<div class="block">
    <label>{'Current image'|i18n( 'design/standard/content/datatype' )}:</label>
    {section show=$attribute_content.original.is_valid}

    <table class="list" cellspacing="0">
        <tr>
            <th class="tight">{'Preview'|i18n( 'design/standard/content/datatype' )}</th>
            <th>{'Filename'|i18n( 'design/standard/content/datatype' )}</th>
            <th>{'MIME type'|i18n( 'design/standard/content/datatype' )}</th>
            <th>{'Size'|i18n( 'design/standard/content/datatype' )}</th>
        </tr>
        <tr>
            <td>{attribute_view_gui image_class=ezini( 'ImageSettings', 'DefaultEditAlias', 'content.ini' ) attribute=$attribute}</td>
            <td>{$attribute.content.original.original_filename|wash( xhtml )}</td>
            <td>{$attribute.content.original.mime_type|wash( xhtml )}</td>
            <td>{$attribute.content.original.filesize|si( byte )}</td>
        </tr>
    </table>

    {* Edit button *}
    <input type="button"
           class="button ezieEdit ezieEditButton"
           name="ezieEdit[{array( "ezie/prepare", $attribute.contentobject_id, $attribute.language_code, $attribute.id, $attribute.version )|implode( '/' )|ezurl( no )}]"
           id="ezieEdit_{$attribute.id}_{$attribute.version}_{$attribute.contentobject_id}"
           value="{'Edit'|i18n( 'design/standard/ezie' )}"
           {if $attribute_content.original.is_valid|not} disabled="disabled"{/if}
           />

           {section-else}
           <p id="msg-fotoware-remove-{$attribute.id}" >{'There is no image file.'|i18n( 'design/standard/content/datatype' )}</p>
    {/section}	

    {section show=$attribute_content.original.is_valid}
    <input class="button"
           type="submit"
           id="btn-fotoware-remove-{$attribute.id}" 
           name="CustomActionButton[{$attribute.id}_delete_image]"
           value="{'Remove image'|i18n( 'design/standard/content/datatype' )}" />
    {section-else}
    <input class="button button-disabled"
           type="submit"
           id="btn-fotoware-remove-{$attribute.id}"
           name="CustomActionButton[{$attribute.id}_delete_image]"
           value="{'Remove image'|i18n( 'design/standard/content/datatype' )}" />
    {/section}
    
    <div id="showPreviewFotoware-{$attribute.id}" style="display:none;">
	    <table class="list" cellspacing="0">
	        <tr>
	            <th class="tight">{'Preview'|i18n( 'design/standard/content/datatype' )}</th>
	            <th>{'Filename'|i18n( 'design/standard/content/datatype' )}</th>
	            <th>{'MIME type'|i18n( 'design/standard/content/datatype' )}</th>
	            <th>{'Size'|i18n( 'design/standard/content/datatype' )}</th>
	        </tr>
	        <tr> 
	            <td> <img id="spf_preview-{$attribute.id}" src="" width="200" style="border: 0px  ;" alt="" title="" /></td>
	            <td> <span id="spf_filename-{$attribute.id}"> </span></td>
	            <td> <span  id="spf_mimetype-{$attribute.id}"> </span></td>
	            <td> <span id="spf_size-{$attribute.id}"> </span></td>
	        </tr>
	    </table>
	  </div>
 
</div>

{* New image file for upload. *}
<div class="block" id="input_file">
    <input type="hidden" name="MAX_FILE_SIZE" value="{$attribute.contentclass_attribute.data_int1|mul( 1024, 1024 )}" />
    <label>{'New image file for upload'|i18n( 'design/standard/content/datatype' )}:</label>
    <input 	id="ezcoa-{if ne( $attribute_base, 'ContentObjectAttribute' )}{$attribute_base}-{/if}{$attribute.contentclassattribute_id}_{$attribute.contentclass_attribute_identifier}_file" 
    		class="box ezcc-{$attribute.object.content_class.identifier} ezcca-{$attribute.object.content_class.identifier}_{$attribute.contentclass_attribute_identifier}" 
    		name="{$attribute_base}_data_imagename_{$attribute.id}" 
    		type="file" />
</div>

<div style="display: none;background: #fff;">
	<div class="fotoware_popup" id="fotoware_popup-{$attribute.id}">
		<div class="fotoware_header_menu">
			<div class="title"><h1>Fotoware</h1></div>
			<!-- <div class="menu"><a href="#" class="show_fotoware_image_select" >Switch Fotoweb image</a> <a href="#" class="show_fotoware_image_edit" >Edit Fotoweb image</a> </div>  -->
		</div>
		
		<div class="fotoware_popup_select" id="fotoware_popup_select-{$attribute.id}" style="display: none;background: #fff;width:750px;height:430px;overflow:hidden;">				
			<div class="content-iframe">
				<iframe src="" style="display:block; width:100%; height:450px; padding:0px; margin:0px; border:0px;background: #fff;"></iframe>
			</div>
		</div>
		
		<div class="fotoware_popup_edit" id="fotoware_popup_edit-{$attribute.id}" style="display: none;background: #fff;width:750px;height:430px;overflow:hidden;">	
			<div class="content-iframe">
				<iframe src="" style="display:block; width:100%; height:450px; padding:0px; margin:0px; border:0px;background: #fff;"></iframe>
			</div>
		</div>				
	</div>
	
</div>

<div class="block">
	<label>{'New image file from FotoWeb'|i18n( 'design/standard/content/datatype' )}:</label>
	<input 
		{if $attribute_content.original.is_valid} disabled="disabled" {/if} 
		value="{'FotoWeb image'|i18n( 'design/standard/content/datatype' )}" name="btn-fotoware" 
		id="button-file-fotoware-{$attribute.id}" 
		class="button button-file-fotoware" 
		type="button"
		ezfw-object-id="{$attribute.contentobject_id}"
		ezfw-attribute-id="{$attribute.id}"
		ezfw-img-file-id="ezcoa-{if ne( $attribute_base, 'ContentObjectAttribute' )}{$attribute_base}-{/if}{$attribute.contentclassattribute_id}_{$attribute.contentclass_attribute_identifier}_file"
		ezfw-img-url_file="{$img_fotoware.info.url_file}"
		ezfw-img-href="{$img_fotoware.info.href}"
		ezfw-attribute-version="{$attribute.version}"
		style="margin-top: 10px;" >
</div>


{* Alternative image text. *}
<div class="block">
    <label>{'Alternative image text'|i18n( 'design/standard/content/datatype' )}:</label>
    <input id="ezcoa-{if ne( $attribute_base, 'ContentObjectAttribute' )}{$attribute_base}-{/if}{$attribute.contentclassattribute_id}_{$attribute.contentclass_attribute_identifier}_alttext" class="box ezcc-{$attribute.object.content_class.identifier} ezcca-{$attribute.object.content_class.identifier}_{$attribute.contentclass_attribute_identifier}" name="{$attribute_base}_data_imagealttext_{$attribute.id}" type="text" value="{$attribute_content.alternative_text|wash(xhtml)}" />
</div>

{/let}
{/default}


{*$attribute|attribute(show)*}
