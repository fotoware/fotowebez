function listener(event){
	
	var url = urlfotowarebasic;
	
	if(event.origin !== url)
		return;
	
	var data = event.data;
	
	if(data.event === 'assetSelected')	
		listimage(data);
	
	if(data.event === 'assetExported')
		exportImage(data);
}

if(window.addEventListener){
	window.addEventListener("message", listener, false);
}else{
	window.attachEvent("onmessage", listener);
}


function listimage(data){

	assetImage = data;	
	showFotowarePopupEdit(assetImage.asset.href);
	
}


function exportImage(data){
	var exported = data.export.export;
	var url_file = exported.image.normal;
	var href = assetImage.asset.href;
	var object_id = $( actionClick ).attr("ezfw-object-id");
	var attribute_id =$( actionClick ).attr("ezfw-attribute-id");
	var buttonFile = $( actionClick ).attr("ezfw-img-file-id");
	var attribute_version = $( actionClick ).attr("ezfw-attribute-version");
	
	
	if( (object_id === "") && (attribute_id === "") && (attribute_version === "") ){
		
		$('#btn-fotoware').colorbox.close();
		
	}else{
		
		var sendInfo = {
		           'object_id': object_id,
		           'attribute_id': attribute_id,
		           'url_file': url_file,
		           'href': href,
		           'version_id': attribute_version,
		           'ftw_type': 'uploadimage',
		           'ftw_action': 'create'		           
		       };

		$.ajax({
		      url: ezroot_url_admin + '/fotoweb/saveobject',
		       type: 'post',
		       data: sendInfo,
		       dataType: 'json',
		       success: function (data){
					if(data.success == true){
						var imageObjectInfo = data.saveobject.ContentObjectAttributeData.DataTypeCustom.alias_list.original;						
						var filesize = bytesToSize(imageObjectInfo.filesize);
						
						$('#spf_preview-'+actionClickId).attr('src', url_file );
						$('#spf_filename-'+actionClickId).text( imageObjectInfo.original_filename);
						$('#spf_mimetype-'+actionClickId).text( imageObjectInfo.mime_type);
						$('#spf_size-'+actionClickId).text( filesize);
						$('#showPreviewFotoware-'+actionClickId).show();
						$('#btn-fotoware-remove-'+actionClickId).hide();
						$('#msg-fotoware-remove-'+actionClickId).hide();
						$("#"+buttonFile).attr("disabled","disabled");
					}
		      }  
		});
		
		$('#btn-fotoware').colorbox.close();
	}
}

function centerIfr( idTag, width, height){
	var widthScreen = $( window ).width();
	var heightScreen = $( window ).height();
	
	var left = parseInt( (widthScreen / 2) - (width / 2) );
	
	$("#"+idTag).css({
		'left':left
	});
	
	//console.log( left +" | "+ widthScreen +" | "+ heightScreen);
}

function getColorBoxFotoware( objfotoware ){
	
	$( '#' + objfotoware ).colorbox({
		inline:true,
		width:"800px",
		height:"600px",
		scrolling: false,
		href:"#fotoware_popup-"+actionClickId,
		overlayClose: false,
		escKey: false,
		closeButton:true,
		fixed: true
	});
	
}


function showFotowarePopupSelect(){	
	$("#fotoware_popup_edit-"+ actionClickId).hide();
	$("#fotoware_popup_size-"+ actionClickId).hide();
	$("#fotoware_popup_select-"+ actionClickId).show();
}

function showFotowarePopupEdit( href ){
	$("#fotoware_popup_edit-"+actionClickId).find('iframe').attr("src", urlwidgetedit + href);
	$("#fotoware_popup_select-"+ actionClickId).hide();
	$("#fotoware_popup_size-"+ actionClickId).hide();
	$("#fotoware_popup_edit-"+ actionClickId).show();
}

function bytesToSize(bytes) {
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (bytes == 0) return 'n/a';
    var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
    if (i == 0) return bytes + ' ' + sizes[i]; 
    return (bytes / Math.pow(1024, i)).toFixed(1) + ' ' + sizes[i];
};

function centerIfr( idTag, width, height){
	var widthScreen = $( window ).width();
	var heightScreen = $( window ).height();
	
	var left = parseInt( (widthScreen / 2) - (width / 2) );
	
	$("#"+idTag).css({
		'left':left
	});
}

$( document ).ready(function() {
		
	//action click button fotoware insert image.
	$('.button-file-fotoware').click(function() {
		
		var button_id = $(this).attr("id");
		actionClick = this;
		actionClickId = $(this).attr("ezfw-attribute-id");
				
		$("#fotoware_popup_select-"+actionClickId).find('iframe').attr("src", urlwidgetselection);
		
		showFotowarePopupSelect();
		getColorBoxFotoware( button_id );
		
	});
	
	var assetImage;
});

