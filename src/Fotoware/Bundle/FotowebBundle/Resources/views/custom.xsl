<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
        version="1.0"
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
        xmlns:xhtml="http://ez.no/namespaces/ezpublish3/xhtml/"
        xmlns:custom="http://ez.no/namespaces/ezpublish3/custom/"
        xmlns:image="http://ez.no/namespaces/ezpublish3/image/"
        exclude-result-prefixes="xhtml custom image">

	<xsl:output method="html" indent="yes" encoding="UTF-8"/>
	
    <xsl:template match="ul">
        <xsl:copy>
	    	<xsl:attribute name="class">
                <xsl:value-of select="concat( @class, ' ', 'list' )"/>
	    	</xsl:attribute>
            <xsl:apply-templates/>
        </xsl:copy>
    </xsl:template>
    

    
    <xsl:template match="custom[@name='sub']">
 		<xsl:choose>
            <xsl:when test="ancestor::header">
               <span>
		             <xsl:copy-of select="@*"/>
		             <xsl:apply-templates/>
		       </span>
            </xsl:when>
            <xsl:otherwise>
            	<sub>
		             <xsl:apply-templates/>
		        </sub>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
</xsl:stylesheet>
