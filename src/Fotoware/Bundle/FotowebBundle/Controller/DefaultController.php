<?php

namespace Fotoware\Bundle\FotowebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('FotowareFotowebBundle:Default:index.html.twig', array('name' => $name));
    }
}
